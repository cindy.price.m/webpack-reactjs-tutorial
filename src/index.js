import React, {Component} from "react";
import ReactDOM from "react-dom";
import { Label } from '@atlaskit/field-base';
import { DatePicker, DateTimePicker, TimePicker } from '@atlaskit/datetime-picker';

export default class TutorialTimePicker extends Component {
    render() {
        return (
            <div>
                <Label label="Date Time Picker" />
                <DatePicker
                    id="datepicker"
                    onChange={console.log}
                    testId={'datePicker'}
                />
            </div>
        );
    }
}
const wrapper = document.getElementById("container");
wrapper ? ReactDOM.render(<TutorialTimePicker />, wrapper) : false;